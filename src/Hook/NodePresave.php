<?php

namespace Drupal\blank_node_title\Hook;

/**
 * @file
 * Contains \Drupal\blank_node_title\Hook\EntityPresave.
 */

/**
 * Controller NodePresave.
 */
class NodePresave {

  /**
   * Hook.
   */
  public static function hook($node) {
    // Set Node-title.
    if (!$node->title->value || $node->title->value == '-') {
      $now = \Drupal::time()->getRequestTime();
      $time = \Drupal::service('date.formatter')->format($now, 'long');
      $title = $node->getType() . " - $time";
      $node->title->setValue($title);
    }
  }

}
