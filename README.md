CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Allow keep NodeTitle field blank and remove Field form node form.

 * For a full description of the module visit:
   https://www.drupal.org/project/blank_node_title

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/blank_node_title


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Blank Node Title Compliance module as you would normally install
   a contributed  Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Forms Settings `admin/config/content/blank-node-title`
       (Administration > Configuration > Content > Blank Node Title).
    3. Set settings & Save.


MAINTAINERS
-----------

 * Anatoly Politsin (APolitsin) - https://www.drupal.org/u/apolitsin

Supporting organization:

 * Synapse-studio - https://www.drupal.org/synapse-studio
